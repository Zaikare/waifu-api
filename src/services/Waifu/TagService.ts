import TagInterface from "@/interfaces/waifu/TagInterface";
import TagObject from "@/objects/waifu/TagObject";

class TagService {
  public static assignTagFromResponseToObjectList(tags: TagInterface[]) {
    const tagList: TagObject[] = [];

    tags.forEach(tag => {
      tagList.push(this.assignTagToObject(tag));
    });

    return tagList;
  }

  private static assignTagToObject(tag: TagInterface): TagObject {
    const tagObject: TagObject = new TagObject();

    tagObject.cardId = tag.cardId;
    tagObject.id = tag.id;
    tagObject.name = tag.name;

    return tagObject;
  }
}
export default TagService;
