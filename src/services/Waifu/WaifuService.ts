import WaifuInterface from "@/interfaces/waifu/WaifuInterface";
import WaifuObject from "@/objects/waifu/WaifuObject";
import ArenaObject from "@/objects/waifu/ArenaObject";
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import TagObject from "@/objects/waifu/TagObject";
import TagService from "@/services/Waifu/TagService";

class WaifuService {
  public static assignResponseToWaifuObject(waifuList: WaifuInterface[]) {
    const waifuListObjects: WaifuObject[] = [];

    if (waifuList.length) {
      waifuList.forEach(waifu => {
        waifuListObjects.push(this.assingWaifuToObject(waifu));
      });
    }

    return waifuListObjects;
  }

  private static assingWaifuToObject(waifu: WaifuInterface): WaifuObject {
    const waifuObject = new WaifuObject();

    const arenaObject = new ArenaObject();
    arenaObject.cardId = waifu.arenaStats.cardId;
    arenaObject.draws = waifu.arenaStats.draws;
    arenaObject.id = waifu.arenaStats.id;
    arenaObject.loses = waifu.arenaStats.loses;
    arenaObject.wins = waifu.arenaStats.wins;

    waifuObject.active = waifu.active;
    waifuObject.affection = waifu.affection;
    waifuObject.arenaStats = arenaObject;
    waifuObject.attack = waifu.attack;
    waifuObject.character = waifu.character;
    waifuObject.creationDate = new Date(waifu.creationDate);
    waifuObject.customBorder = waifu.customBorder;
    waifuObject.customImage = waifu.customImage;
    waifuObject.defence = waifu.defence;
    waifuObject.dere = waifu.dere;
    waifuObject.expCnt = waifu.expCnt;
    waifuObject.firstIdOwner = waifu.firstIdOwner;
    waifuObject.gameDeckId = waifu.gameDeckId;
    waifuObject.health = waifu.health;
    waifuObject.id = waifu.id;
    waifuObject.image = waifu.image;
    waifuObject.inCage = waifu.inCage;
    waifuObject.isTradable = waifu.isTradable;
    waifuObject.lastIdOwner = waifu.lastIdOwner;
    waifuObject.name = waifu.name;
    waifuObject.rarity = waifu.rarity;
    waifuObject.rarityOnStart = waifu.rarityOnStart;
    waifuObject.restartCnt = waifu.restartCnt;
    waifuObject.source = waifu.source;
    waifuObject.starStyle = waifu.starStyle;

    if (waifu.tagList.length) {
      waifuObject.tagList = TagService.assignTagFromResponseToObjectList(
        waifu.tagList
      );
    } else {
      waifuObject.tagList = [];
    }

    waifuObject.title = waifu.title;
    waifuObject.unique = waifu.unique;
    waifuObject.upgradesCnt = waifu.upgradesCnt;

    return waifuObject;
  }

  private static assingTags(tagsString: string | undefined): TagObject[] {
    const tagsList: TagObject[] = [];
    if (tagsString == null || tagsString.length === 0) {
      return tagsList;
    }

    const explodedTags = tagsString.split(" ");
    explodedTags.forEach((tagData: string) => {
      const tag: TagObject = new TagObject();
      tag.name = tagData;
      tagsList.push(tag);
    });

    return tagsList;
  }
}

export default WaifuService;
