import UserInterface from '@/interfaces/user/UserInterface'
import UserObject from '@/objects/user/UserObject'

class UserService {
  public static assignUserFromResponseToObjectList(users: UserInterface[]) {
    const userList: UserObject[] = [];

    users.forEach(user => {
      userList.push(this.assignTagToObject(user));
    });

    return userList;
  }

  private static assignTagToObject(user: UserInterface): UserObject {
    const userObject: UserObject = new UserObject();

    userObject.id = user.id;
    userObject.name = user.name;
    userObject.avatarUrl = user.avatarUrl;
    userObject.rank = user.rank;

    return userObject;
  }
}
export default UserService;
