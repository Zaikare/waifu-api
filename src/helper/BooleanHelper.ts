class BooleanHelper {
  public static translateBoolean(variable: boolean) {
    return variable ? "Tak" : "Nie";
  }
}

export default BooleanHelper;
