class SortingHelper {
  static compareString(a: string, b: string) {
    const first = a.toLowerCase();
    const second = b.toLowerCase();

    if (first < second) {
      return -1;
    }

    if (first > second) {
      return 1;
    }

    return 0;
  }

  static compareNumber(a: number, b: number) {
    if (a < b) {
      return -1;
    }

    if (a > b) {
      return 1;
    }

    return 0;
  }
}

export default SortingHelper;
