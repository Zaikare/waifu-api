enum SortEnum {
  NAME,
  RANK,
  TITLE,
  HEALTH,
  ATTACK,
  DEFENSE,
  ID
}
export default SortEnum;