enum RarityEnum {
  e,
  d,
  c,
  b,
  a,
  s,
  ss,
  sss
}

export default RarityEnum;
