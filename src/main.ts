import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import VueClipboards from "vue-clipboards";
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import("../node_modules/vuetify/dist/vuetify.min.css");
Vue.config.productionTip = false;

Vue.use(VueClipboards);

new Vue({
  router,
  store,

  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  vuetify,
  render: h => h(App)
}).$mount("#app");
