import { getters } from "./getters";
import { actions } from "./actions";
import { mutations } from "./mutations";
import { WaifuState } from "./types";

export const state: WaifuState = {
  copyArray: []
};

const namespaced = true;

export const waifu: any = {
  namespaced,
  state,
  getters,
  actions,
  mutations
};
