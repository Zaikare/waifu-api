import { ActionTree } from "vuex";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const actions: ActionTree<any, any> = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  updateCopy({ commit, state }): any {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const payload: any = state;
    commit("updateCopy", payload);
  },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  deleteCopy({ commit, state }): any {
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    const payload: number = state;
    commit("deleteCopy", payload);
  }
};
