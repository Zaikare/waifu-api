import { WaifuState, CopyArray } from "./types";

export const getters: any = {
  copyArray(state: WaifuState): CopyArray[] {
    return state.copyArray;
  }
};
