// eslint-disable-next-line @typescript-eslint/no-explicit-any
import { CopyArray, WaifuState } from "@/store/waifu/types";

export const mutations: any = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  updateCopy(state: WaifuState, payload: CopyArray) {
    if (state.copyArray.length === 0) {
      state.copyArray.push(payload);
      return;
    }

    const index = state.copyArray.findIndex((waifu) => {
      return waifu.id === payload.id;
    });
    if (-1 < index) {
      state.copyArray.splice(index, 1, payload);
    } else {
      state.copyArray.push(payload);
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  deleteCopy(state: WaifuState, payload: number) {
    const index = state.copyArray.findIndex(waifu => {
      return waifu.id === payload;
    });
    if (-1 < index) {
      state.copyArray.splice(index, 1);
    }
  }
};
