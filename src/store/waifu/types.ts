export interface WaifuState {
  copyArray: CopyArray[];
}

export interface CopyArray {
  clicked: boolean;
  id: number;
  name: string;
}
