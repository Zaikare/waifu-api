import Vue from "vue";
import Vuex from "vuex";
import { waifu } from './waifu/index';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    waifu
  }
});
