import Vue from "vue";
import TagChoiceInterface from "@/interfaces/waifu/TagChoiceInterface";
import SendSortingInterface from "@/interfaces/core/SendSortingInterface";
import ClickedCardInterface from "@/interfaces/core/ClickedCardInterface";

export const EventBus = new Vue({
  methods: {
    sendTagData: (tags: string[]) => {
      EventBus.$emit("tag-sendTag", tags);
    },
    sendTagChoice: (tags: TagChoiceInterface[]) => {
      EventBus.$emit("tag-choice", tags);
    },
    resetTagChoice: () => {
      EventBus.$emit("tag-reset", true);
    },
    sendSearchData: (searchValue: string | null) => {
      EventBus.$emit("search-value", searchValue);
    },
    sendSortingData: (sorting: SendSortingInterface) => {
      EventBus.$emit("sorting-sendSorting", sorting);
    },
    sendPickedCardStatus: (cardPickStatus: ClickedCardInterface) => {
      EventBus.$emit("card-sendPickedStatus", cardPickStatus);
    }
  }
});
