import SortEnum from "@/enum/SortEnum";

class SortingObject {
  public sortingDictionary: string[] = [];

  constructor() {
    this.sortingDictionary[SortEnum.NAME] = "Nazwa";
    this.sortingDictionary[SortEnum.RANK] = "Ranga";
    this.sortingDictionary[SortEnum.TITLE] = "Tytuł";
    this.sortingDictionary[SortEnum.HEALTH] = "Pkt. zdrowia";
    this.sortingDictionary[SortEnum.ATTACK] = "Atak";
    this.sortingDictionary[SortEnum.DEFENSE] = "Obrona";
    this.sortingDictionary[SortEnum.ID] = "Id";
  }
}

export default SortingObject;
