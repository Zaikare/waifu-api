class RarityObject {
  public static rarityDictionary: any = {
    sss: 7,
    ss: 6,
    s: 5,
    a: 4,
    b: 3,
    c: 2,
    d: 1,
    e: 0
  };
}

export default RarityObject;
