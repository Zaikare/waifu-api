import ArenaObject from "@/objects/waifu/ArenaObject";
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import TagObject from "@/objects/waifu/TagObject";

class WaifuObject {
  private _id!: number;
  private _active!: boolean;
  private _inCage!: boolean;
  private _isTradable!: boolean;
  private _expCnt!: number;
  private _affection!: number;
  private _upgradesCnt!: number;
  private _restartCnt!: number;
  private _rarity!: string; // object
  private _rarityOnStart!: string; // object
  private _dere!: string; // object
  private _defence!: number;
  private _attack!: number;
  private _health!: number;
  private _name!: string;
  private _character!: number;
  private _creationDate!: Date;
  private _source!: string; // object
  private _title!: string;
  private _tagList!: TagObject[];
  private _image: string | undefined;
  private _customImage!: null;
  private _firstIdOwner!: number;
  private _lastIdOwner!: number;
  private _unique!: boolean;
  private _starStyle!: string; // object
  private _customBorder!: boolean;
  private _arenaStats!: ArenaObject;
  private _gameDeckId!: number;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get active(): boolean {
    return this._active;
  }

  set active(value: boolean) {
    this._active = value;
  }

  get inCage(): boolean {
    return this._inCage;
  }

  set inCage(value: boolean) {
    this._inCage = value;
  }

  get isTradable(): boolean {
    return this._isTradable;
  }

  set isTradable(value: boolean) {
    this._isTradable = value;
  }

  get expCnt(): number {
    return this._expCnt;
  }

  set expCnt(value: number) {
    this._expCnt = value;
  }

  get affection(): number {
    return this._affection;
  }

  set affection(value: number) {
    this._affection = value;
  }

  get upgradesCnt(): number {
    return this._upgradesCnt;
  }

  set upgradesCnt(value: number) {
    this._upgradesCnt = value;
  }

  get restartCnt(): number {
    return this._restartCnt;
  }

  set restartCnt(value: number) {
    this._restartCnt = value;
  }

  get rarity(): string {
    return this._rarity;
  }

  set rarity(value: string) {
    this._rarity = value;
  }

  get rarityOnStart(): string {
    return this._rarityOnStart;
  }

  set rarityOnStart(value: string) {
    this._rarityOnStart = value;
  }

  get dere(): string {
    return this._dere;
  }

  set dere(value: string) {
    this._dere = value;
  }

  get defence(): number {
    return this._defence;
  }

  set defence(value: number) {
    this._defence = value;
  }

  get attack(): number {
    return this._attack;
  }

  set attack(value: number) {
    this._attack = value;
  }

  get health(): number {
    return this._health;
  }

  set health(value: number) {
    this._health = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get character(): number {
    return this._character;
  }

  set character(value: number) {
    this._character = value;
  }

  get creationDate(): Date {
    return this._creationDate;
  }

  set creationDate(value: Date) {
    this._creationDate = value;
  }

  get source(): string {
    return this._source;
  }

  set source(value: string) {
    this._source = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get tagList(): TagObject[] {
    return this._tagList;
  }

  set tagList(value: TagObject[]) {
    this._tagList = value;
  }

  get image(): string | undefined {
    return this._image;
  }

  set image(value: string | undefined) {
    this._image = value;
  }

  get customImage(): null {
    return this._customImage;
  }

  set customImage(value: null) {
    this._customImage = value;
  }

  get firstIdOwner(): number {
    return this._firstIdOwner;
  }

  set firstIdOwner(value: number) {
    this._firstIdOwner = value;
  }

  get lastIdOwner(): number {
    return this._lastIdOwner;
  }

  set lastIdOwner(value: number) {
    this._lastIdOwner = value;
  }

  get unique(): boolean {
    return this._unique;
  }

  set unique(value: boolean) {
    this._unique = value;
  }

  get starStyle(): string {
    return this._starStyle;
  }

  set starStyle(value: string) {
    this._starStyle = value;
  }

  get customBorder(): boolean {
    return this._customBorder;
  }

  set customBorder(value: boolean) {
    this._customBorder = value;
  }

  get arenaStats(): ArenaObject {
    return this._arenaStats;
  }

  set arenaStats(value: ArenaObject) {
    this._arenaStats = value;
  }

  get gameDeckId(): number {
    return this._gameDeckId;
  }

  set gameDeckId(value: number) {
    this._gameDeckId = value;
  }

  public getArrayOfTagName() {
    if (this.tagList == undefined || this.tagList.length === 0) {
      return [];
    }

    const stringTagList: string[] = [];

    this.tagList.forEach((tag: TagObject) => {
      stringTagList.push(tag.name);
    });

    return stringTagList;
  }
}

export default WaifuObject;
