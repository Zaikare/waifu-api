class TagObject {
  // eslint-disable-next-line prettier/prettier
  private _id!: number;
  private _cardId!: number;
  private _name!: string;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get cardId(): number {
    return this._cardId;
  }

  set cardId(value: number) {
    this._cardId = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
}

export default TagObject;
