class ArenaObject {
  private _id!: number;
  private _wins!: number;
  private _loses!: number;
  private _draws!: number;
  private _cardId!: number;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get wins(): number {
    return this._wins;
  }

  set wins(value: number) {
    this._wins = value;
  }

  get loses(): number {
    return this._loses;
  }

  set loses(value: number) {
    this._loses = value;
  }

  get draws(): number {
    return this._draws;
  }

  set draws(value: number) {
    this._draws = value;
  }

  get cardId(): number {
    return this._cardId;
  }

  set cardId(value: number) {
    this._cardId = value;
  }
}

export default ArenaObject;
