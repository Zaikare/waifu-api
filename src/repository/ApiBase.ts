import axios from "axios";

class ApiBase {
  private baseUrl = "http://api.waifu.zaikare.eu";

  public async get(url: string) {
    const result = await axios.get(this.baseUrl + url);
    if (result.status == 200) {
      return result.data;
    }
  }
}
export default ApiBase;
