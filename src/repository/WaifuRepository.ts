import ApiBase from "@/repository/ApiBase";

class WaifuRepository extends ApiBase {
  public getUserCards(userId: number) {
    return super.get(`/user/${userId}/list`);
  }
}

export default WaifuRepository;
