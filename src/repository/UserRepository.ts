import ApiBase from "@/repository/ApiBase";

class UserRepository extends ApiBase {
  public findUser(userName: string) {
    return super.get(`/user/find/${userName}`);
  }
}

export default UserRepository;
