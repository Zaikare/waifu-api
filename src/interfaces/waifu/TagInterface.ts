interface TagInterface {
  id: number;
  cardId: number;
  name: string;
}
export default TagInterface;
