interface TagChoiceInterface {
  choice: string | null;
  id: string;
}
export default TagChoiceInterface;
