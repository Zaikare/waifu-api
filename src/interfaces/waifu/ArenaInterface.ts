interface ArenaInterface {
  id: number;
  wins: number;
  loses: number;
  draws: number;
  cardId: number;
}
export default ArenaInterface;