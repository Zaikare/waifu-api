import ArenaInterface from "@/interfaces/waifu/ArenaInterface";
import TagInterface from "@/interfaces/waifu/TagInterface";

interface WaifuInterface {
  id: number;
  active: boolean;
  inCage: boolean;
  isTradable: boolean;
  expCnt: number;
  affection: number;
  upgradesCnt: number;
  restartCnt: number;
  rarity: string; // object
  rarityOnStart: string; // object
  dere: string; // object
  defence: number;
  attack: number;
  health: number;
  name: string;
  character: number;
  creationDate: string;
  source: string; // object
  title: string;
  tagList: TagInterface[];
  image: string | undefined;
  customImage: null;
  firstIdOwner: number;
  lastIdOwner: number;
  unique: boolean;
  starStyle: string; // object
  customBorder: boolean;
  arenaStats: ArenaInterface;
  gameDeckId: number;
}
export default WaifuInterface;
