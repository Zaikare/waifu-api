interface UserInterface {
  id: number;
  name: string;
  avatarUrl: string;
  rank: string;
}
export default UserInterface;
