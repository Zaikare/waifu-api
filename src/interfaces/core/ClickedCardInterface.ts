interface ClickedCardInterface {
  clicked: boolean;
  id: number;
  name: string;
}

export default ClickedCardInterface;
