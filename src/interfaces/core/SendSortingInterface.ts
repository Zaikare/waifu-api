interface SendSortingInterface {
  sortId: number;
  sortArray: number[];
}

export default SendSortingInterface;
